<?php

$height = [170, 160, 150, 180, 190];
$students = ['Petrov', 'Ivanov', 'Sidorov', 'Fedorov', 'Vlasov'];

$max = max($height);

$result = $students[array_search($max, $height)];

print $result . "\n";
