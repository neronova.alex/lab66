<?php

print "Please enter the number: \n";
$number = trim(fgets(STDIN));

function countConsecutive($n)
{
    $count = 0;
    for ($i = 1; $i * ($i + 1) < 2 * $n; $i++) {
        $a = (int)(1.0 * $n - ($i * (int)($i + 1)) / 2) / ($i + 1);
        if ($a - (int)$a == 0.0) {
            $count++;
        }
    }

    return $count;
}

print countConsecutive($number) . "\n";
