<?php

$numbers = [2,4,6,8,10];

function isArithmeticProgression($array) {
    $delta = $array[1] - $array[0];
    for($i = 2; $i < count($array); $i++) {
        if(($array[$i] - $array[$i - 1]) != $delta) {
            return null;
        }
    }

    return $delta;
}

var_export(isArithmeticProgression($numbers));
print "\n";
