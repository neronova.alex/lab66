<?php

print "Please enter number A: \n";
$a = trim(fgets(STDIN));
print "Please enter number N: \n";
$n = trim(fgets(STDIN));

$result = 0;

for($i = 0; $i <= $n; $i++) {
    $result += $i * $a;
}

print $result . "\n";