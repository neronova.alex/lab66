<?php

print "Please enter the first string \n";
$first = trim(fgets(STDIN));
print "Please enter the second string \n";
$second = trim(fgets(STDIN));

$first = str_split($first);
$second = str_split($second);

$symbol = 0;

if($first == $second) {
    print "Strings are matched \n";
}

for($i = 0; $i < count($first); $i++) {
    if($first[$i] == $second[$i]) {
        $symbol += 1;
    } else {
        break;
    }
}

print $symbol . "\n";